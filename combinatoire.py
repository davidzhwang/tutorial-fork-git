# coding=utf-8
#----------------------------------------------------------
# Fichier: combinatoire.py    Auteur(s): Simon DÉSAULNIERS
# Date: 2013-07-16
#----------------------------------------------------------
# Exercice de gestion de code avec git.
#----------------------------------------------------------

def fact(n):
    """Calcul la factorielle de l'entier n"""
    if n == 1 or n == 0:
        return 1
    elif n > 1:
        return n*fact(n-1)
    else:
        raise Exception("La factorielle n'est définie que pour les entiers positifs!")
#         
#         
# 
def cat(n):
	return fact(2*n)/fact(n+1)/fact(n)

def fibo(n):
	""" Calcul du nombre de fibonacci"""
	if n==0:
		return 0
	elif n==1:
		return 1
	elif n>1:
		return fibo(n-1)+fibo(n-2)
	else:
		raise Exception("Le nombre de fibonacci est défini pour n supérieur ou égal à 0.")
	

#TODO: Écrire une fonction qui génère les nombres de catalan.
def binom1(n,k):
	if n>=0 and k>=0:
		return fact(n)/(fact(n-k)*fact(k))
	else:
		raise Exception("Le coefficient binomial est défini pour deux nombres entiers positifs (0 inclu)")
def binom2m(n,k)
    if n>=0 and k==0:
        return 1
    elif k>n:
        return 0
    else: return binom2(n-1,k-1)+binom2(n-1,k)
